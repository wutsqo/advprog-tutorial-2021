package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
    }

    @Override
    public void update() {
        Quest quest = guild.getQuest();
        String questType = guild.getQuestType();
        if (questType.equals("D") || questType.equals("R") || questType.equals("E"))
            this.getQuests().add(quest);
    }
}
